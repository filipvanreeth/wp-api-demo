<div <?php echo get_block_wrapper_attributes(); ?>>
	<?php
	require_once plugin_dir_path( __DIR__ ) . 'includes/class-dogs.php';
	$dogs = new Dogs();
	$dogs->render_dogs();
	?>
</div>
