<?php

class Dogs {

	public function get_dogs() {
		$api_args = array(
			'headers'   => array(
				'Authorization' => 'Basic ' . base64_encode( 'dog_lover:nQNF 9XgO n4Tj RB0z 9STk CQ0I' ),
			),
			'sslverify' => false,
		);

		$response = wp_remote_get( 'https://wordpress-bedrock.lndo.site/wp-json/wp-api-demo/v1/dogs/all', $api_args );

		if ( is_wp_error( $response ) ) {
			return false;
		}

		$body = json_decode( wp_remote_retrieve_body( $response ) );

		return $body;
	}

	public function render_dogs() {
		$dogs = $this->get_dogs();

		if ( ! $dogs ) {
			echo 'No dogs found';
			return;
		}

		ob_start();
		?>
		<style>

		</style>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		echo $output;

		echo '<div class="dog-list">';

		foreach ( $dogs as $dog ) {
			$dog_name    = $dog->name;
			$dog_sex     = $dog->sex;
			$dog_breed   = $dog->breed;
			$breed_name  = $dog_breed->name ?? '';
			$breed_image = $dog_breed->image ?? '';
			?>
			<div class="dog">
				<img src="<?php echo esc_url( $breed_image ); ?>" alt="<?php echo $breed_name; ?>" srcset="">
				<h2><?php echo esc_html( $dog_name ); ?></h2>
				<h4><?php echo esc_html( $breed_name ); ?></h4>
				<p><?php echo esc_html( $dog_sex ); ?></p>
			</div>
			<?php
		}

		echo '</div>';
	}
}
