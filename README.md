# WordPress API Demo

This repository holds two folders:
- A block editor dogs-block - This will use a GET request to get and show all the dogs via a 'Dogs' block.
- The WP API Demo - This holds the setup of the dog and dog breeds endpoints. In this folder you will also find an `api-requests.http` file. This file has som demo requests. Please keep in mind that the authorization credentials are not valid anymore. To get this working you need to regenerate an application password for the admin user.

Need help? Just contact me via filip@filipvanreeth.com.

This Git repository is part af a WordPress talk. You can find the presentation [here](https://docs.google.com/presentation/d/1bI2ZEN8OOTWQz1N60cZUgqCh15ygVyLgbnM-205Vtkk/edit#slide=id.p.).
Need help? Just contact me via filip@filipvanreeth.com.
