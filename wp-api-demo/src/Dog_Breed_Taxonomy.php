<?php

namespace FiliWP\WP_API_Demo;

use WP_CLI;

class Dog_Breed_Taxonomy {
	public const TAXONOMY = 'dog_breed';

	public function __construct() {
		add_action( 'init', [ $this, 'register_taxonomy' ] );
		add_action( 'cli_init', [ $this, 'cli_init' ] );
		add_filter( 'manage_edit-' . self::TAXONOMY . '_columns', [ $this, 'add_custom_column' ] );
		add_filter( 'manage_' . self::TAXONOMY . '_custom_column', [ $this, 'render_custom_column' ], 10, 3 );
		add_action( 'rest_api_init', [ $this, 'register_rest_route' ] );
	}

	public function register_rest_route() {
		register_rest_route( 'wp-api-demo/v1', '/dog-breeds', [ 
			'methods' => 'GET',
			'callback' => [ $this, 'api_get_all_dog_breeds' ],
			'permission_callback' => function () {
				return current_user_can( 'read' );
			}
		] );
	}

	public function api_get_all_dog_breeds() {
		$dog_breeds = get_terms( [ 
			'taxonomy' => self::TAXONOMY,
			'hide_empty' => false,
		] );

		$response = [];

		foreach ( $dog_breeds as $dog_breed ) {
			$dogs = Dog_Post_Type::get_dogs_by_breed( $dog_breed->slug );

			$response[] = [ 
				'id' => $dog_breed->term_id,
				'name' => $dog_breed->name,
				'slug' => $dog_breed->slug,
				'image' => self::get_image_by_slug( $dog_breed->slug )['url'] ?? null,
				'dogs' => $dogs,
			];
		}

		return rest_ensure_response( $response );
	}

	public function register_taxonomy() {
		register_taxonomy( self::TAXONOMY, Dog_Post_Type::POST_TYPE, [ 
			'labels' => [ 
				'name' => 'Dog Breeds',
				'singular_name' => 'Dog Breed',
				'add_new_item' => 'Add New Dog Breed',
			],
			'public' => true,
			'show_in_rest' => true,
		] );
	}

	public static function insert_dog_breeds() {
		$dog_breeds = [ 
			'Beagle',
			'Belgian Shepherd',
			'Bulldog',
			'Chihuahua',
			'Dalmatian',
			'German Shepherd',
			'Golden Retriever',
			'Labrador Retriever',
			'Poodle',
			'Siberian Husky',
		];

		foreach ( $dog_breeds as $dog_breed ) {
			if ( term_exists( $dog_breed, self::TAXONOMY ) ) {
				continue;
			}

			wp_insert_term( $dog_breed, self::TAXONOMY );
		}
	}

	public static function get_random_dog_breed(): ?\WP_Term {
		$dog_breeds = get_terms( [ 
			'taxonomy' => self::TAXONOMY,
			'hide_empty' => false,
		] );

		if ( ! $dog_breeds ) {
			return null;
		}

		$random_dog_breed = $dog_breeds[ array_rand( $dog_breeds ) ];

		return $random_dog_breed;
	}

	public static function get_image_by_slug( $slug ): ?array {
		$image = 'assets/dist/images/' . $slug;
		// @phpstan-ignore-next-line
		$image_path = WP_API_DEMO_PLUGIN_DIR_PATH . $image;
        // @phpstan-ignore-next-line
		$image_url = WP_API_DEMO_PLUGIN_DIR_URL . $image . '.webp';

		return array(
			'path' => file_exists( $image_path ) ? $image_path : null,
			'url' => $image_url,
		);
	}

	public static function cli_insert_dog_breeds() {
		self::insert_dog_breeds();
		WP_CLI::success( 'Dog breeds inserted' );
	}

	public function cli_init() {
		WP_CLI::add_command( 'dog-breeds insert', [ $this, 'cli_insert_dog_breeds' ] );
	}

	public static function get_default_rest_api_endpoint( $id = null ) {
		return get_rest_url( null, 'wp/v2/' . self::TAXONOMY . ( $id ? '/' . $id : '' ) );
	}

	public function add_custom_column( $columns ) {
		$columns['rest_api_endpoints'] = __( 'REST API Endpoints', 'wp-api-demo' );

		return $columns;
	}

	public function render_custom_column( $empty, $custom_column, $term_id ) {
		if ( $custom_column == 'rest_api_endpoints' ) {
			$render = '<a href="' . self::get_default_rest_api_endpoint( $term_id ) . '" class="button" target="_blank">' . __( 'Default', 'wp-api-demo' ) . '</a>';
			echo $render;
		}
	}
}