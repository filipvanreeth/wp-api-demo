<?php

namespace FiliWP\WP_API_Demo;

use WP_CLI;

class Dog_Post_Type {
	public const POST_TYPE = 'dog';

	public function __construct() {
		add_action( 'init', [ $this, 'register_post_type' ] );
		add_action( 'add_meta_boxes', [ $this, 'dog_settings_meta_box' ] );
		add_action( 'save_post', [ $this, 'save_dog_details' ] );
		add_filter( 'manage_edit-dog_columns', [ $this, 'add_dog_breed_column' ] );
		add_action( 'manage_dog_posts_custom_column', [ $this, 'display_dog_breed_column' ], 10, 2 );
		add_action( 'cli_init', [ $this, 'cli_init' ] );
		add_action( 'rest_api_init', [ $this, 'register_rest_route' ] );
	}

	public function register_rest_route() {
        // Get all dogs
		register_rest_route( 'wp-api-demo/v1', '/dogs/all', [ 
			'methods' => 'GET',
			'callback' => [ $this, 'api_get_all_dogs' ],
			'permission_callback' => function () {
				return current_user_can( 'read' );
			}
		] );

        // Get dog by ID
		register_rest_route( 'wp-api-demo/v1', '/dogs/(?P<id>\d+)', [ 
			'methods' => 'GET',
			'callback' => [ $this, 'api_get_dog' ],
			'args' => [ 
				'id' => [ 
					'validate_callback' => function ($param, $request, $key) {
						return is_numeric( $param );
					}
				],
			],
			'permission_callback' => function () {
				return current_user_can( 'manage_options' );
			}
		] );

        // Create dog
		register_rest_route( 'wp-api-demo/v1', '/dogs/create', [ 
			'methods' => 'POST',
			'callback' => [ $this, 'api_post_dog' ],
			'permission_callback' => function () {
				return current_user_can( 'manage_options' );
			}
		] );
	}

	public function api_get_dog( \WP_REST_Request $request ) {
		$dog_id = $request->get_param( 'id' );

		$dog = get_post( $dog_id );

		if ( ! $dog ) {
			return new \WP_Error( 'dog_not_found', 'Dog not found', [ 'status' => 404 ] );
		}

		$dog_breed = get_the_terms( $dog->ID, 'dog_breed' )[0] ?? null;

		$response = [ 
			'id' => $dog->ID,
			'name' => $dog->post_title,
			'sex' => get_post_meta( $dog->ID, 'dog_sex', true ) ?: null,
			'breed' => array(
				'name' => get_the_terms( $dog->ID, 'dog_breed' )[0]->name ?? 'Unknown',
				'image' => Dog_Breed_Taxonomy::get_image_by_slug( $dog_breed->slug )['url'] ?? null,
			)
		];

		return rest_ensure_response( $response );
	}

	public function api_post_dog( \WP_REST_Request $request ) {
		$dog_name = $request->get_param( 'dog_name' );
		$dog_sex = $request->get_param( 'dog_sex' ) ?? null;
		$dog_breed_id = $request->get_param( 'dog_breed_id' );

		if ( ! $dog_name || ! $dog_breed_id ) {
			return new \WP_Error( 'missing_data', 'Missing data', [ 'status' => 400 ] );
		}

		if ( ! in_array( $dog_sex, array( 'male', 'female' ) ) ) {
			return new \WP_Error( 'invalid_dog_sex', 'Invalid dog sex', [ 'status' => 400 ] );
		}

		$post_args = array(
			'post_title' => $dog_name,
			'post_type' => self::POST_TYPE,
			'post_status' => 'publish',
		);

		if ( $dog_sex ) {
			$post_args['meta_input'] = array( 'dog_sex' => $dog_sex );
		}

        /** @var \WP_Error|int */
		$dog_id = wp_insert_post( $post_args );

		if ( is_wp_error( $dog_id ) ) {
			return new \WP_Error( 'insert_error', 'Error inserting dog', [ 'status' => 500 ] );
		}

		$dog_breed_term = get_term_by( 'id', $dog_breed_id, 'dog_breed' );
		$dog_breed_taxonomy = Dog_Breed_Taxonomy::TAXONOMY;

		wp_set_object_terms( $dog_id, $dog_breed_term->term_id, $dog_breed_taxonomy, true );

		return rest_ensure_response( [ 
			'message' => 'Dog inserted',
			'data' => [ 
				'dog_id' => $dog_id,
				'dog_name' => $dog_name,
				'dog_sex' => $dog_sex,
				'dog_breed_id' => $dog_breed_id,
			]
		] );
	}

	public function api_get_all_dogs( \WP_REST_Request $request ) {
		$dogs = get_posts( [ 
			'post_type' => self::POST_TYPE,
			'post_status' => 'publish',
			'numberposts' => -1,
		] );

		$response = [];

		foreach ( $dogs as $dog ) {
			$dog_id = $dog->ID;
			$dog_breed = get_the_terms( $dog_id, 'dog_breed' )[0] ?? null;
            $dog_breed_image = Dog_Breed_Taxonomy::get_image_by_slug( $dog_breed->slug )['url'];
            
			$response[] = [ 
				'id' => $dog_id,
				'name' => $dog->post_title,
				'sex' => get_post_meta( $dog_id, 'dog_sex', true ) ?: null,
				'breed' => array(
					'name' => get_the_terms( $dog_id, 'dog_breed' )[0]->name ?? 'Unknown',
					'image' => $dog_breed_image,
				)
			];
		}

		return rest_ensure_response( $response );
	}

	public function register_post_type() {
		register_post_type( self::POST_TYPE, [ 
			'labels' => [ 
				'name' => 'Dogs',
				'singular_name' => 'Dog',
				'add_new' => 'Add New Dog',
				'add_new_item' => 'Add New Dog',
				'new_item' => 'New Dog',
			],
			'public' => true,
			'has_archive' => true,
			'show_in_rest' => true,
			'taxonomies' => [ 'dog_breed' ],
			'supports' => [ 'title', 'editor', 'author' ],
		] );
	}

	public function dog_settings_meta_box() {
		add_meta_box(
			'dog_settings_meta_box',
			'Dog Settings',
			[ $this, 'render_dog_settings_meta_box' ],
			self::POST_TYPE
		);
	}

	public function render_dog_settings_meta_box( $post ) {
		echo '<h4>Sex:</h4>';
		$sex = get_post_meta( $post->ID, 'dog_sex', true );
		$sex_male_checked = ( $sex == 'male' ) ? 'checked' : '';
		$sex_female_checked = ( $sex == 'female' ) ? 'checked' : '';
		echo '<label><input type="radio" name="dog_sex" value="male" ' . $sex_male_checked . '> Male</label><br>';
		echo '<label><input type="radio" name="dog_sex" value="female" ' . $sex_female_checked . '> Female</label>';
		wp_nonce_field( 'save_dog_details', 'dog_details_nonce' );
	}

	public function save_dog_details( $post_id ) {
		if ( ! isset( $_POST['dog_details_nonce'] ) ) {
			return $post_id;
		}

		$nonce = $_POST['dog_details_nonce'];

		if ( ! wp_verify_nonce( $nonce, 'save_dog_details' ) ) {
			return $post_id;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		$dog_sex_post_key = 'dog_sex';
		$dog_sex_post_data = $_POST[ $dog_sex_post_key ];
		$new_dog_sex = ( isset( $dog_sex_post_data ) ? sanitize_text_field( $dog_sex_post_data ) : '' );

		update_post_meta( $post_id, $dog_sex_post_key, $new_dog_sex );
	}

	public function add_dog_breed_column( $columns ) {
		$columns['dog_breed'] = 'Dog Breed';
		return $columns;
	}

	public function display_dog_breed_column( $column, $post_id ) {
		if ( $column == 'dog_breed' ) {
			$terms = get_the_term_list( $post_id, 'dog_breed', '', ', ', '' );
			echo $terms;
		}
	}

	public static function insert_dogs() {
		$dog_names = [ 
			'Buddy',
			'Max',
			'Charlie',
			'Jack',
			'Cooper',
			'Rocky',
			'Bear',
			'Duke',
			'Toby',
			'Tucker',
		];

		foreach ( $dog_names as $dog_name ) {

			if ( self::dog_exists( $dog_name ) ) {
				continue;
			}

			$args = array(
				'post_title' => $dog_name,
				'post_type' => self::POST_TYPE,
				'post_status' => 'publish',
			);

            /** @var \WP_Error|int $dog_id */
			$dog_id = wp_insert_post( $args );

			if ( is_wp_error( $dog_id ) ) {
				continue;
			}

			$term = Dog_Breed_Taxonomy::get_random_dog_breed()->name;
			$taxonomy = Dog_Breed_Taxonomy::TAXONOMY;

			wp_set_object_terms( $dog_id, $term, $taxonomy, true );

		}
	}

	public static function delete_dogs() {
		$dogs = get_posts( [ 
			'post_type' => self::POST_TYPE,
			'post_status' => 'any',
			'numberposts' => -1,
		] );

		foreach ( $dogs as $dog ) {
			wp_delete_post( $dog->ID, true );
		}
	}

	public static function dog_exists( $dog_name ) {
		return get_page_by_title( $dog_name, OBJECT, self::POST_TYPE ) ? true : false;
	}

	public function cli_init() {
		WP_CLI::add_command( 'dogs insert', [ $this, 'cli_insert_dogs' ] );
		WP_CLI::add_command( 'dogs delete', [ $this, 'cli_delete_dogs' ] );
	}

	/**
	 * Inserts dogs
	 * @return void
	 */
	public static function cli_insert_dogs() {
		self::insert_dogs();
		WP_CLI::success( 'Dogs inserted' );
	}

	/**
	 * Deletes dogs
	 * @return void
	 */
	public static function cli_delete_dogs() {
		self::delete_dogs();
		WP_CLI::success( 'Dogs deleted' );
	}

	public static function get_dogs_by_breed( string $breed ) {
		$dogs = get_posts( [ 
			'post_type' => self::POST_TYPE,
			'post_status' => 'publish',
			'numberposts' => -1,
			'tax_query' => [ 
				[ 
					'taxonomy' => Dog_Breed_Taxonomy::TAXONOMY,
					'field' => 'slug',
					'terms' => $breed,
				]
			]
		] );

		if ( ! $dogs ) {
			return [];
		}

		$dogs = array_map( function ($dog) {
			return [ 
				'id' => $dog->ID,
				'name' => $dog->post_title,
			];
		}, $dogs );

		return $dogs;
	}
}