<?php
use FiliWP\WP_API_Demo\Dog_Breed_Taxonomy;
/*
 * Plugin Name:       WordPress API Demo
 * Version:           0.1.0
 * Requires at least: 6.0
 * Requires PHP:      7.4
 * Author:            Filip Van Reeth
 * Author URI:        https://filipvanreeth.com
 * License:           GPL v2 or later
 * Text Domain:       wp-api-demo
 * Domain Path:       /languages
 */

define( 'WP_API_DEMO_PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ) );
define( 'WP_API_DEMO_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );

require_once __DIR__ . '/src/Dog_Post_Type.php';
$dog_post_type = new FiliWP\WP_API_Demo\Dog_Post_Type();

require_once __DIR__ . '/src/Dog_Breed_Taxonomy.php';
$dog_breed = new FiliWP\WP_API_Demo\Dog_Breed_Taxonomy();